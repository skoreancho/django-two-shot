from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from django.db import models
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
@login_required
def receipts_list(request):
    receipts_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts_list": receipts_list}
    return render(request, "receipts/receipts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {"create": form}

    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    list = ExpenseCategory.objects.filter(owner=request.user)

    context = {"category": list}

    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    list = Account.objects.filter(owner=request.user)

    context = {"account": list}

    return render(request, "receipts/accounts.html", context)


@login_required
def create_categories(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            form.save()
            return redirect("categories")
    else:
        form = ExpenseCategoryForm()

    context = {"create_category": form}

    return render(request, "receipts/create_categories.html", context)


@login_required
def create_accounts(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            form.save()
            return redirect("accounts")
    else:
        form = AccountForm()

    context = {"create_account": form}

    return render(request, "receipts/create_accounts.html", context)
