from django.urls import path
from receipts.views import (
    receipts_list,
    create_receipt,
    category_list,
    account_list,
    create_categories,
    create_accounts,
)

urlpatterns = [
    path("", receipts_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="categories"),
    path("accounts/", account_list, name="accounts"),
    path("categories/create/", create_categories, name="create_category"),
    path("accounts/create/", create_accounts, name="create_account"),
]
